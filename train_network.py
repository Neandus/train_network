# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Raport z wynikami problemu pobrania i wizualizacji danych.
# 
# Zgromadzanie danych i wizualizacja połączeń kolejowych z miast wojewódzkich.
# Dane zostały pobrane przy użyciu metody scrappingu internetowego ze strony https://bilkom.pl/stacje/tablica z zastosowaniem biblioteki Selenium.
# Dane zwizualizowane zostały przy użyciu biblioteki NetworkX.
# Lista głównych stacji kolejowych dla miast wojewódzkich została załadowana z pliku CSV.
# 
# Przyjęte założenia:
# * Dataset tworzą połączenia mieszczące się w granicach jedej doby, kolejnej względem dnia uruchomienia programu programu.
# * Wizualizacja połączeń nie oddaje rozmieszczenia geograficznego miast na mapie Polski.
# * Lista miast wojewódzkich znajduje się w pliku stations.csv.
# * Po zebraniu danych zostaną one zapisane do pliku csv - trains_dataset.csv.
# * Połączenia wychodzące z danego miasta na grafie będą posiadały jednakowy kolor .
# 
# %% [markdown]
# ## Załadowanie listy nazw stacji w miastach wojewódzkich.

# %%
import csv

stations = []

with open("stations.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        stations.append(row[0])

# %% [markdown]
# ## Implementacja parsera strony bilkom.pl o nazwie PageParser
# Główną metodą tej klasy jest **parse_station**

# %%
from time import sleep
from datetime import datetime
from datetime import timedelta
from datetime import date
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium import webdriver


page_url = 'https://bilkom.pl/stacje/tablica'

PAUSE_TIME_SHORT = 0.1
PAUSE_TIME_LONG = 5


class PageParser():
    def __init__(self, url):
        self.url = url
        self.browser = None
        self.dataset = []

    ### Funkcja zamieniająca miesiąc na liczbę.
    ### Wejściowy łańcuch znaków odpowiada konwencji przyjętej prze autorów strony.
    def month_name_to_number(self, month_string):
        month_lookup = {
            "sty": 1, "lut": 2, "mar": 3, "kwi": 4,
            "maj": 5, "cze": 6, "lip": 7, "sie": 8,
            "wrz": 9, "paź": 10, "lis": 11, "gru": 12}

        s = month_string.strip()[:3].lower()
        return month_lookup[s]

    ### Zebranie danych z konkretnej podstrony rozkładu jazdy.
    ### Bazując na rozkładzie jazdy dla wybranej stacji pobierz dane o:
    ### mieście odjazdu, mieście przyjazdu, dacie odjazdu, godzinie odjazdu, przewoźniku oraz numerze pociągu
    def parse_timetable_subpage(self, station, timetable):

        desired_trip_date = date.today() + timedelta(days=1)

        timetable_children = timetable.find_elements(by=By.CSS_SELECTOR, value="li")
        for li in timetable_children:
            dt_wrapper = li.find_element(by=By.CSS_SELECTOR, value="div[class='trip']")                .find_element(by=By.CSS_SELECTOR, value="div[class='date-time-wrapper']")

            trip_date_raw = dt_wrapper                .find_element(by=By.CSS_SELECTOR, value="div[class='day']")                .text

            year = date.today().year
            day, month = trip_date_raw.split(" ")

            trip_date = date(year, self.month_name_to_number(month), int(day))

            # Uwzględnij tylko połączenia z żądanego dnia (tj. kolejnego dnia od daty dzisiejszej)
            if(trip_date == desired_trip_date):

                direction = li.find_element(
                    by=By.CSS_SELECTOR, value="div[class='direction']").text

                trip_time = dt_wrapper.find_element(
                    by=By.CLASS_NAME, value="time").text

                # Skrótową nazwę użytkownika można wyłuskać z nazwy pliku z logo.
                carrier_img = li.find_element(by=By.CSS_SELECTOR, value="div[class='carrier']")                    .find_element(by=By.CSS_SELECTOR, value="img")                    .get_attribute("src")
                carrier = carrier_img.split('/')[-1].split('.')[0].upper()

                number = li.find_element(
                    by=By.CSS_SELECTOR, value="div[class='number']").text

                self.dataset.append(
                    {
                        'OrigStationName': str(station),
                        'DestStationName': str(direction),
                        'DepartureDate': str(trip_date),
                        'DepartureTime': str(trip_time),
                        'Carrier': str(carrier),
                        'TrainNumber': str(number)
                    }
                )

    ### Przejście do następnej podstrony
    def go_next_subpage(self, current_page, subpages):

        for child in subpages:
            if child.text == '...':
                pass
            elif (int(current_page) + 1) == int(child.text):
                current_page = int(child.text)
                child.find_element(by=By.CSS_SELECTOR,
                                   value="a[class='page']").click()
                break

        # aktualizacja listy połączeń dla danej podstrony
        subpages = self.browser.find_element(by=By.CSS_SELECTOR, value="ul[class='pagination']")            .find_elements(by=By.CSS_SELECTOR, value="li")

        return current_page, subpages

    ### Scrapping rozkładu jazdy dla danej stacji początkowej
    def parse_timetable(self, station):

        subpages = self.browser.find_element(by=By.CSS_SELECTOR, value="ul[class='pagination']")            .find_elements(by=By.CSS_SELECTOR, value="li")

        subpages_count = len(subpages)
        subpages_total = -1

        if(subpages_count > 0):
            subpages_total = int(subpages[-1].text)
        else:
            return

        current_page = 1

        sleep(PAUSE_TIME_SHORT)

        while current_page < subpages_total:
            timetable = self.browser.find_element(by=By.CSS_SELECTOR, value="ul[id='timetable']")
            self.parse_timetable_subpage(station, timetable)
            current_page, subpages = self.go_next_subpage(current_page, subpages)
            sleep(PAUSE_TIME_SHORT)

    ### Ustawienie czasu - poelga na przeglądaniu dostępnych wartości z wyskakującego okienka z godziną.
    ### Implementacja strony bilkom.pl nie pozwala na bezpośrednie wpisanie żądanej godziny do pola 'input id=time'
    def setup_time(self, hour, minutes):
        self.browser.find_element(
            by=By.CSS_SELECTOR, value="input[id='time']").click()
        sleep(PAUSE_TIME_SHORT)
        self.browser.find_element(
            by=By.CSS_SELECTOR, value="span[class='timepicker-hour']").click()
        sleep(PAUSE_TIME_SHORT)
        hours_table = self.browser            .find_element(by=By.CSS_SELECTOR, value="div[class='timepicker-hours']")            .find_elements(by=By.CSS_SELECTOR, value="tr")

        hour_to_click = None
        for tr in hours_table:
            tr_children = tr.find_elements(by=By.CSS_SELECTOR, value="*")
            for td in tr_children:
                if int(td.text) == hour:
                    hour_to_click = td
                    break
        sleep(PAUSE_TIME_SHORT)
        hour_to_click.click()

        sleep(PAUSE_TIME_SHORT)
        self.browser.find_element(
            by=By.CSS_SELECTOR, value="span[class='timepicker-minute']").click()
        sleep(PAUSE_TIME_SHORT)
        minutes_table = self.browser.find_element(
            by=By.CSS_SELECTOR, value="div[class='timepicker-minutes']").find_elements(by=By.CSS_SELECTOR, value="tr")

        minute_to_click = None
        for tr in minutes_table:
            tr_children = tr.find_elements(by=By.CSS_SELECTOR, value="*")
            for td in tr_children:
                if int(td.text) == minutes:
                    minute_to_click = td
                    break
        sleep(PAUSE_TIME_SHORT)
        minute_to_click.click()

    def clear_text(self, element):
        length = len(element.get_attribute('value'))
        element.send_keys(length * Keys.BACKSPACE)

    def scroll_page_up(self):
        self.browser.execute_script("window.scrollTo(0, 0)")
        sleep(PAUSE_TIME_LONG)

    def scroll_page_down(self):
        # Pobranie wysokości przewijania
        last_height = self.browser.execute_script(
            "return document.body.scrollHeight;")

        while True:
            # Przewinięcie do samego dołu widocznej części strony
            self.browser.execute_script(
                "window.scrollTo(0, document.body.scrollHeight);")

            # Oczekiwanie na załadaowanie strony
            sleep(PAUSE_TIME_LONG)

            # Obliczenie nowej wysokości przeiwjania oraz porównianie z poprzednią wartością
            new_height = self.browser.execute_script(
                "return document.body.scrollHeight;")
            # Jeśli wartość nie ulega zmianie przerwij pętlę
            if new_height == last_height:
                break
            last_height = new_height

    ### Funkcja przygotowująca stronę do scrappingu.
    def parse_station(self, station):

        # Załadowanie opcji bez uruchamiania okna przeglądarki
        # W celu wizualizacji procesu scrappingu w oknie przeglądarki można zakomentować kolejne dwie linie.
        browserOptions = FirefoxOptions()
        browserOptions.add_argument("--headless")

        with webdriver.Firefox(options=browserOptions) as browser:
            self.browser = browser
            self.browser.get(self.url)

            # przewinięcie strony do samej góry - w celu załadowania okna z nazwą stacji
            self.scroll_page_up()

            input_from_station = self.browser.find_element(
                by=By.CSS_SELECTOR, value="input[id='fromStation']")
            input_from_station.click()

            # zresetowanie nazwy stacji
            self.clear_text(input_from_station)
            sleep(2)

            # ustawienie godziny najbliższej kolejnemu dniu
            self.setup_time(23, 55)

            sleep(2)
            # ustawienie nazwy stacji
            input_from_station.send_keys(station)

            sleep(2)
            self.browser.find_element(
                by=By.CSS_SELECTOR, value="button[id='search-btn']").click()
            # sleep(2)

            # przewinięcie strony do samego dołu - w celu załadowania zawartości listy podstron
            self.scroll_page_down()
            sleep(PAUSE_TIME_LONG)

            # Rozpoczęcie scrappingu rozkładu jazdy danej stacji
            self.parse_timetable(station)

# %% [markdown]
# ## Zakmnięcie funkcjonalności obiektu klasy PageParser w jednej funkcji.

# %%
def getDataSet(station):
    pp = PageParser(page_url)
    pp.parse_station(station)
    sleep(PAUSE_TIME_SHORT)
    return pp.dataset

# %% [markdown]
# ## Wywołanie funkcji getDataSet w
# Ze względu na ilość opóźnień w kodzie(wywołań funkcji sleep) oraz ilość danych do zebrania(nawet 40 podstron dla stacji) zebranie danych zajmuje długi czas - około 16 minut.
# Dzięki wprowadzeniu obsługi wieloprocesowej (4 procesy) udało się zredukować czas do około 4 minut - 4 krotny zysk.
# Z kolei 8 procesów ukończy pracę w 2min 40s.

# %%
import multiprocessing

dataset = None

if __name__ == '__main__':
    pool = multiprocessing.Pool(processes=8)
    dataset = pool.map(getDataSet, stations)

# %% [markdown]
# ## Zebrane dane należy przechować w sposób umożliwiający późniejsze wykorzystanie.
# Umożliwia to również rozwój drugiej części skryptu (wizualizacji danych) bez konieczności oczekiwania na ponowne zebranie danych ze strony.

# %%
import csv

keys = dataset[0][0].keys()

with open('trains_dataset.csv', 'w', newline='') as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    for entry in dataset:
        dict_writer.writerows(entry)

# %% [markdown]
# ## Ponowne wczytanie danych z pliku csv powoduje zredukowanie jednego poziomu zagnieżdżenia - z listy list słowników do listy słowników.

# %%
import csv
trains_dataset = []

with open("trains_dataset.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        trains_dataset.append(row)

# %% [markdown]
# ## Wizualizacja danych w postaci grafu uzyskanego przy pomocoy biblioteki NetworkX.
# %% [markdown]
# ### Załadowanie niezbędnych bibliotek: NetowrkX, Matplotlib, GraphViz.

# %%
from matplotlib import pylab
import matplotlib.pyplot as plt
import networkx as nx
import graphviz as gv

### Funkcja zapisująca graf do pliku o rozszerzeniu png
def save_graph(graph, file_name):
    nx.drawing.nx_pydot.write_dot(stations_graph, file_name)
    gv.render('dot', 'png', file_name)

# %% [markdown]
# ### Tabela kolorów dla połączeń między węzłami dla danego węzła (stacji odjazdu)

# %%
colors = {
    'Białystok': "red",
    'Warszawa Centralna': "blue",
    'Bydgoszcz Główna': "green",
    'Gdańsk Główny': "greenyellow",
    'Gorzów Wielkopolski': "magenta",
    'Katowice': "cyan",
    'Kielce': "sienna",
    'Kraków Główny': "black",
    'Lublin Główny': "pink",
    'Łódź Fabryczna': "orchid",
    'Olsztyn Główny': "orange",
    'Opole Główne': "coral",
    'Poznań Główny': "violet",
    'Rzeszów Główny': "blueviolet",
    'Szczecin Główny': "aquamarine",
    'Toruń Główny': "salmon",
    'Wrocław Główny': "plum",
    'Zielona Góra Główna': "fuchsia"
}


# %%
# Usunięcie nagłówka datasetu
trains_dataset = trains_dataset[1:]

stations_graph = nx.Graph()

# Lista wszystkich krawędzi
edges = [(entry[0], entry[1]) for entry in trains_dataset]

# Lista unikalnych krawędzi
unique_edges = list(set([tuple(sorted(e)) for e in edges]))

# Dodawaj krawędzie w raz z zadanym kolorem dla stacji odjazdu
for station in stations:
    for orig, dest in unique_edges:
        if orig == station:
            stations_graph.add_edge(orig, dest, color=colors[station])

# Zabieg pozwalający zachować kolejnośc kolorów w węzłach
colors = nx.get_edge_attributes(stations_graph, 'color').values()

plt.figure(figsize=(50, 50))
plt.title('Graf reprezentujący połączenia kolejowe wychodzące z miast wojewódzkich w Polsce', size=50)

pos = nx.spring_layout(stations_graph)
nx.draw(stations_graph, pos, edge_color=colors, node_size=10, node_color='black')

nx.draw_networkx_labels(stations_graph, pos,
                        {n: str(n) for n in stations_graph.nodes()},
                        font_size=16)

save_graph(stations_graph, "result_graph")

# %% [markdown]
# ## Możliwe ścieżki rozwoju:
# * Wybranie konkretnej daty odjazdu
# * Odwzorowanie położenia węzła (stacji) na mapie.

